-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 22 2020 г., 05:10
-- Версия сервера: 10.3.22-MariaDB
-- Версия PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `test`
--

-- --------------------------------------------------------

--
-- Структура таблицы `authors`
--

CREATE TABLE `authors` (
  `author_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `patronymic` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `authors`
--

INSERT INTO `authors` (`author_id`, `name`, `surname`, `patronymic`) VALUES
(1, 'віфвіфвіф', 'віфвіфвфі', 'віфвіфвіф'),
(2, 'выыфвыф', 'ывавыав', 'ыавыавыавы'),
(3, 'dsadsa', 'dsadsadsa', 'fdsfdsfds'),
(4, 'dfdsfdsfds', 'fdsfdsf', 'dsfdsfdsf'),
(5, 'выфвыф', 'выфвывфыв', 'ыфвыфвыфвыф'),
(6, 'вфавіаві', 'авіавіа', 'віавіавіавіаві'),
(7, 'fdsfdsgsdfdsf', 'dsfdsgfdgfd', 'dsfdsgfdgsfds'),
(8, 'вфыафыа', 'ыавыпарпа', 'рпарапрпарпарпа'),
(9, 'dsafdsg', 'dfhgfh', 'gfjgtrhtrh'),
(10, 'авіавіа', 'віфвіфвіф', 'рорпорпорпорпорп'),
(11, 'рпарпар', 'пааіваіаві', 'акуцкуцкуцкуц'),
(12, 'куцкуцкуцк', 'уцкуцкуцку', 'цкуцкуцкуцкцу'),
(13, 'авпавпаврпа', 'рпарпар', 'паоимтмитми'),
(14, 'арпарпар', 'паравпавп', 'авпаврпапор'),
(15, 'рорпорпо', 'рпорпопропр', 'орпопрорпорп'),
(16, 'rerwerewrew', 'rewrwerwerewr', 'ewrewrewrwe');

-- --------------------------------------------------------

--
-- Структура таблицы `author_book`
--

CREATE TABLE `author_book` (
  `book_id` bigint(20) UNSIGNED NOT NULL,
  `author_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `author_book`
--

INSERT INTO `author_book` (`book_id`, `author_id`) VALUES
(1, 9),
(2, 1),
(3, 14),
(4, 1),
(5, 1),
(5, 2),
(6, 1),
(6, 2),
(6, 3),
(7, 2),
(7, 3),
(8, 1),
(8, 2),
(8, 3),
(9, 1),
(9, 2),
(9, 3),
(10, 1),
(10, 2),
(11, 12),
(11, 13),
(11, 14),
(12, 1),
(12, 2),
(13, 3),
(14, 1),
(14, 2),
(15, 1),
(16, 1),
(16, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `books`
--

CREATE TABLE `books` (
  `book_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publication_date` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `books`
--

INSERT INTO `books` (`book_id`, `name`, `short_description`, `img`, `publication_date`) VALUES
(1, 'ававваываываываывыа', 'аывавыавыавыаыв', 'images/uXl3cVb0L5WIHWS76G3HOQMlY88VgPBvKyAdIAQO.png', 312321),
(2, 'ыфвыфв', 'ыфвыфвыфвыф', 'images/AxEFJgajDocBotTUCY8ucZ00R5erg6wdA43zj5aV.png', 31221),
(3, 'выфвыфв', 'ыфвыфвыфвф', 'images/tZNAOsM4XJAGJy6EbyFDImMYNerXRPhWQY53MN32.png', 32123),
(4, 'dsasadsad', NULL, 'images/YyPfdWtna1TFXad08Z0SHrS99Kb6j7fkCDBfyXsM.png', 23123),
(5, 'dfdsfdsfdsf', 'dsfdsfdsfdsfds', 'images/yr4rE5yRUSkMjJJ39vdoIuzoFuQhqRhm6YL7HLxG.png', 321312),
(6, 'dfsdsafdsaf', 'dsafdsafdsafsadf', 'images/p7RDtvzAcn3thZ5nmbFE6ybsKAiJZGw5QyKTPnGy.png', 23123),
(7, 'авывыавыавы', 'авыавыавыавы', 'images/Az1A5KP7DfcEhBZTaNJI2PXvoWTZ7XkJEyU4S5A8.png', 2321321),
(8, 'ывфавыфа', 'вфыавфыавф', 'images/pOKD6v8s4Irurqi92PUx2epOh5mgaKoBfjNdwUaq.png', 23131),
(9, 'аыфвыфа', 'выфавфыафы', 'images/Pzgl1W0ERQrnKmAwf1apMnwydR3yww0AEOyGN3U6.png', 321321213),
(10, 'ыавфвыа', 'ывафавыфаыф', 'images/11JUgEZKeiukXzGLk6chVUSO65AFf0KXmgSe6i8c.png', 231312),
(11, 'фавфыа', 'ыфваыфвафвы', 'images/hXFIweDXfD1dFuIMa55d5NoVVaRWOH0IFFWFdKxJ.png', 312321),
(12, 'ыфаввыфа', 'фыафафвыаф', 'images/TOTBsIv8Ienv894Re5xgZ5lcCPH3Zc0wjfNlw2QI.png', 312312),
(13, 'павыпавы', 'пыавпыав', 'images/1osf0Xabl1Z8EZ836G2FL3fjPV5ur6N6sQCmOhry.png', 321321),
(14, 'вфыавыфа', 'выфавыфавыф', 'images/EKVfWswEwa15PnpIoNddEeMEbgO0QfLUvWXInUC8.png', 321321),
(15, 'впапы', 'пвыпвыпв', 'images/InE9NORBJnanNDdEuPu2s4fCfSoAQxYKEFwhMskQ.png', 324324),
(16, 'авфыавфыа', 'фыафыафы', 'images/EkT4NVI2GzYjvsgM7MZIYMTjqeeAT2kPuSlTbrT6.png', 312321);

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2020_06_11_095420_create_books_table', 1),
(2, '2020_06_11_122038_create_authors_table', 1),
(3, '2020_06_11_123946_create_author_book_table', 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `authors`
--
ALTER TABLE `authors`
  ADD PRIMARY KEY (`author_id`),
  ADD UNIQUE KEY `authors_author_id_unique` (`author_id`);

--
-- Индексы таблицы `author_book`
--
ALTER TABLE `author_book`
  ADD KEY `author_book_book_id_foreign` (`book_id`),
  ADD KEY `author_book_author_id_foreign` (`author_id`);

--
-- Индексы таблицы `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`book_id`),
  ADD UNIQUE KEY `books_book_id_unique` (`book_id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `authors`
--
ALTER TABLE `authors`
  MODIFY `author_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT для таблицы `books`
--
ALTER TABLE `books`
  MODIFY `book_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `author_book`
--
ALTER TABLE `author_book`
  ADD CONSTRAINT `author_book_author_id_foreign` FOREIGN KEY (`author_id`) REFERENCES `authors` (`author_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `author_book_book_id_foreign` FOREIGN KEY (`book_id`) REFERENCES `books` (`book_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
