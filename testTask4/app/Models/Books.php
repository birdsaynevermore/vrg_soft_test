<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Books extends Model
{

    protected $table = 'Books';
    protected $primaryKey = 'book_id';
    public $timestamps = false;
    protected $fillable = ['name', 'short_description', 'img',  'publication_date'];


    public function authors()
    {
        return $this->belongsToMany(Authors ::class, 'author_book', 'book_id', 'author_id');
    }

}
