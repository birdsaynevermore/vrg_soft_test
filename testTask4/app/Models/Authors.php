<?php

    namespace App\Models;

    use Illuminate\Database\Eloquent\Model;

    class Authors extends Model
    {
        protected $table = 'Authors';
        protected $primaryKey = 'author_id';
        public $timestamps = false;
        protected $fillable = ['name', 'surname', 'patronymic'];


        public function books()
        {
            return $this->belongsToMany(Authors ::class, 'author_book', 'author_id', 'book_id');
        }
    }
