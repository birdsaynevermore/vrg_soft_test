<?php

    namespace App\Http\Controllers;


    use App\Models\Authors;
    use App\Models\Books;
    use Illuminate\Http\Request;
    use Illuminate\Http\Response;

    class BooksController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return Response
         */
        public function index()
        {
            $books = Books::paginate(15);
            return view('Books.index', compact('books'));
        }

        /*compact('books')*/
        /**
         * Show the form for creating a new resource.
         *
         * @return Response
         */
        public function create()
        {
            $authors = Authors::all();
            return view('Books.create', compact('authors'));
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @return Response
         */
        public function store(Request $request)
        {
            $request->validate(
                [
                    'name' => 'required',
                    'short_description',
                    'img' => 'mimes:jpeg,png| max:2048',
                    'publication_date' => 'numeric'
                ]
            );


            $book = new Books(
                [
                    'name' => $request->get('name'),
                    'short_description' => $request->get('short_description'),
                    'img' => $request->file('img')->store('images', 'public'),
                    'publication_date' => $request->get('publication_date')

                ]
            );

            $authors = $request->get('authors');

            $book->save();
            foreach ($authors as $author) {
                $book->authors()->attach($author);
            }


            return redirect('/Books')->with('success', 'Book successfully added!');
        }

        /**
         * Display the specified resource.
         *
         * @param \App\books $post
         * @return Response
         */
        public function show($book)
        {
            $book = Books::find($book);

            return view('Books.show', compact('book'))->render();
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param \App\books $post
         * @return Response
         */
        public function edit($book_id)
        {
            $authors = Authors::all();
            $book = Books::find($book_id);

            return view('Books.edit', ['authors' => $authors, 'book' => $book])->render();
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param $book_id
         * @return Response
         */
        public function update(Request $request, $book_id)
        {
            $request->validate(
                [
                    'name' => 'required',
                    'short_description',
                    'img' => 'mimes:jpg,png| max:2048',
                    'publication_date' => 'numeric'
                ]
            );
            $book = Books::find($book_id);
            $book->name = $request->get('name');
            $book->short_description = $request->get('short_description');
            $book->publication_date = $request->get('publication_date');

            if ($request->get('img')) {
                $book->img = $request->file('img')->store('images', 'public');
            }

            $book->authors()->detach();

            $book->save();


            $book->authors()->attach($request->get('authors'));


            return redirect('/Books')->with('success', 'Book successfully edited!');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param $book_id
         * @return Response
         */
        public function destroy($book_id)
        {
            $book_id = Books::find($book_id);
            $book_id->delete();

            return redirect('/Books')->with('success', 'Book deleted!');
        }


        public function searchBooks(Request $request)
        {
            $search = $request->get('search');
            $books = Books::with('authors')->where('name', 'LIKE', '%' . $search . '%')->paginate(5);
            return view('Books.index', ['books' => $books]);
        }


        public function searchAuthors(Request $request)
        {
            $search = $request->get('search');
            $books = Books::whereHas(
                'authors',
                function ($q) use ($search) {
                    $q->where('name', 'LIKE', '%' . $search . '%')
                        ->orWhere('surname', 'LIKE', '%' . $search . '%')
                        ->orWhere('patronymic', 'LIKE', '%' . $search . '%');
                }
            )->paginate(5);


            return view('Books.index', ['books' => $books]);
        }


        public function sortByName(Request $request)
        {
            $books = Books::with('Authors')
                ->orderBy('name', 'asc')
                ->paginate(15);
            return view('Books.index', compact('books'));
        }

    }
