<?php

    namespace App\Http\Controllers;

    use App\Models\authors;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;

    class AuthorsController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $authors = authors::paginate(15);

            return view('Authors.index', compact('authors'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('Authors.create');
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            $request->validate(
                [
                    'name' => 'required | alpha',
                    'surname' => 'required | min:3 | alpha',
                    'patronymic' => 'alpha'
                ]
            );

            $author = new authors(
                [
                    'name' => $request->get('name'),
                    'surname' => $request->get('surname'),
                    'patronymic' => $request->get('patronymic')
                ]
            );

            $author->save();

            return redirect('/Authors')->with('success', 'Author successfully added');
        }

        /**
         * Display the specified resource.
         *
         * @param \App\Models\authors $authors
         * @return \Illuminate\Http\Response
         */
        public function show($author)
        {
            $author = Authors::find($author);
            return view('Authors.show', compact('author'));
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param \App\Models\authors $authors
         * @return \Illuminate\Http\Response
         */
        public function edit($author_id)
        {
            $author = Authors::find($author_id);

            return view('Authors.edit', compact('author'))->render();
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param \App\Models\authors $authors
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $author_id)
        {
            $request->validate(
                [
                    'name' => 'required|alpha',
                    'surname' => 'required | min:3 | alpha',
                    'patronymic' => 'alpha'
                ]
            );

            $author = Authors::find($author_id);
            $author->name = $request->get('name');
            $author->surname = $request->get('surname');
            $author->patronymic = $request->get('patronymic');

            $author->save();

            return redirect('/Authors')->with('success', 'Author successfully edited!')->render();
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param \App\Models\authors $authors
         * @return \Illuminate\Http\Response
         */
        public function destroy($author_id)
        {
            $author_id = Authors::find($author_id);
            $author_id->delete();

            return redirect('/Authors')->with('success', 'Author deleted!');
        }

        public function searchAuthorName(Request $request)
        {
            $search = $request->get('search');
            $authors = DB::table('authors')->where('name', 'LIKE', '%' . $search . '%')->paginate(5);
            return view('Authors.index', ['authors' => $authors]);
        }

        public function searchAuthorSurname(Request $request)
        {
            $search = $request->get('search');
            $authors = DB::table('authors')->where('surname', 'LIKE', '%' . $search . '%')->paginate(5);
            return view('Authors.index', ['authors' => $authors]);
        }

        public function sortBySurname(Request $request)
        {
            $authors = DB::table('Authors')
                ->orderBy('surname', 'asc')->paginate(5);
            return view('Authors.index', compact('authors'));
        }


    }
