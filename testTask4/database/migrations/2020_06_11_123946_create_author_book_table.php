<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;

    class CreateAuthorBookTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create(
                'author_book',
                function (Blueprint $table) {
                    $table->unsignedBigInteger('book_id');
                    $table->unsignedBigInteger('author_id');

                    $table->foreign('book_id')->references('book_id')->on('Books')->onDelete('cascade');
                    $table->foreign('author_id')->references('author_id')->on('Authors')->onDelete('cascade');
                }
            );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('booksAuthors');
        }
    }
