@extends('layouts.app')

@section('title', 'Add author')

@section('content')
    <nav class="navbar navbar-expand-lg navbar-dark bg-success mb-3 navbar-toggleable   ">
        <a class="navbar-brand" href="#">Book Reference</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02"
                aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item custom-control-inline">
                    <a class="nav-link" href="{{ route('Books.index') }}">Books</a>
                    <a class="nav-link" href="{{ route('Authors.index') }}">Authors</a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="row">
        <div class="col-lg-6 mx-auto">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li> {{$error}}</li>
                        @endforeach
                    </ul>
                </div><br>
            @endif
            <form enctype="multipart/form-data" method="POST" action="{{ route('Authors.store') }}">
                @csrf
                <div class="form-group">
                    <label for="book-name">Author name</label>
                    <input required type="text" name="name" class="form-control" placeholder="Author name"
                           id="author-name"
                           value=" {{ old('name') }}">
                </div>
                <div class="form-group">
                    <label for="author-surname">Author surname</label>
                    <input required type="text" name="surname" class="form-control" placeholder="Author surname"
                           id="author-surnme"
                           value=" {{ old('surname') }}">
                </div>
                <div class="form-group">
                    <label for="book-patronymic">Author patronymic</label>
                    <input required type="text" name="patronymic" class="form-control"
                           placeholder="Author patronymic" id="author-patronymic"
                           value=" {{ old('patronymic') }}">
                </div>
                <button type="submit" class="btn btn-success">Add author</button>
            </form>
        </div>
    </div
@endsection
