@extends('layouts.app')

@section('title', 'Add author')

@section('content')
    <div class="row">
        <div class="col-lg-6 mx-auto">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li> {{$error}}</li>
                        @endforeach
                    </ul>
                </div><br>
            @endif
            <form enctype="multipart/form-data" method="POST" action="{{ route('Authors.update', $author )}}">
                @csrf
                @method('PATCH ')
                <div class="form-group">
                    <label for="book-name">Author name</label>
                    <input required type="text" name="name" class="form-control" placeholder="Author name"
                           id="author-name"
                           value=" {{ $author->name }}">
                </div>
                <div class="form-group">
                    <label for="author-surname">Author surname</label>
                    <input required type="text" name="surname" class="form-control" placeholder="Author surname"
                           id="author-surnme"
                           value=" {{ $author->surname }}">
                </div>
                <div class="form-group">
                    <label for="book-patronymic">Author patronymic</label>
                    <input required type="text" name="patronymic" class="form-control"
                           placeholder="Author patronymic" id="author-patronymic"
                           value=" {{ $author->patronymic }}">
                </div>
                <button type="submit" class="btn btn-success">Edit author</button>
            </form>
        </div>
    </div
@endsection
