@extends('layouts.app')

@section('title', 'Show author')

@section('content')

    <div class="card">
        <div class="card-body">
            <h3>Name: {{ $author->name }}</h3>
            <b><p>Surname: {{ $author->surname  }}</p></b>
            <p>Patronymic: {{ $author->patronymic}}</p>
        </div>
    </div>

@endsection
