@extends('layouts.app')

@section('title', 'Show book')

@section('content')

    <div class="card">
        <div class="card-body">

            <h3>Name: {{ $book->name }}</h3>
            <p>Short description:{{ $book->short_description }}</p>
            <p>Img:
                <img width="200" height="200" src=" {{ asset('/storage/' . $book->img)}} " alt="Book-img">
            </p>
            <p>Author: @foreach($book->authors as $author)
                    {{ $author->surname . ' ' . $author->name . ' ' . $author->patronymic . '; '}}
                @endforeach</p>
            <p><b>Publication date: {{ $book->publication_date }}</b></p>
        </div>
    </div>

@endsection
