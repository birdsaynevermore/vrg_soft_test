@extends('layouts.app')

@section('title', 'Edit book')

@section('content')
    <div class="row">
        <div class="col-lg-6 mx-auto">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li> {{$error}}</li>
                        @endforeach
                    </ul>
                </div><br>
            @endif
            <form enctype="multipart/form-data" method="POST" action="{{ route('Books.update', $book) }}">
                @csrf
                @method('PATCH ')
                <div class="form-group">
                    <label for="book-name">Book's name</label>
                    <input required type="text" name="name" class="form-control" placeholder="Name" id="book-name"
                           value=" {{ $book->name  }}">
                </div>
                <div class="form-group">
                    <label for="book-shortDescription">Short description</label>
                    <textarea class="form-control" name="short_description" rows="3" placeholder="Short description"
                              id="book-shortDescription">{{ $book->short_description }}</textarea>
                </div>
                <div>
                    <p>Img:<img width="200" height="200" src=" {{ asset('/storage/' . $book->img)}} " alt="Book-img">
                    </p>
                    <p>Upload img</p>
                    <input type="file" name="img" multiple accept="image/jpeg,image/png" id="book-img">

                </div>
                <div class="form-group">
                    <label for="author">Select Authors:</label>
                    <br>
                    @foreach ($authors as $author)
                        <input type="checkbox" name="authors[]" id="author-id" value="{{ $author->author_id }}">
                        <label
                            for="author-id"> {{ $author->surname . ' ' .  $author->name . ' ' . $author->patronymic  }}</label>
                        <br>
                    @endforeach

                </div>
                <div class="form-group">
                    <label for="book-pubdate">Publication date</label>
                    <input required type="text" name="publication_date" class="form-control"
                           placeholder="Publication date" id="publication_date"
                           value=" {{ $book->publication_date }}">
                </div>
                <button type="submit" class="btn btn-success">Edit book</button>
            </form>
        </div>
    </div

@endsection
