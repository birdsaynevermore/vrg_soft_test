@extends('layouts.app')

@section('title', 'Books')

@section('content')
    <nav class="navbar navbar-expand-lg navbar-dark bg-success mb-3 navbar-toggleable   ">
        <a class="navbar-brand" href="#">Book Reference</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02"
                aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item custom-control-inline">
                    <a class="nav-link" href="{{ route('Books.index') }}">Books</a>
                    <a class="nav-link" href="{{ route('Authors.index') }}">Authors</a>
                </li>
            </ul>
        </div>
    </nav>


    <a href="{{ route('Books.create') }}" class="btn-outline-success display-4">Add book</a>

    @if(session()->get('success'))
        <div class="alert-success">
            {{ session()->get('success') }}
        </div>
    @endif

    <form action="/search_books" method="GET" class="form-inline my-2 my-lg-0 p-2">
        <label>
            <input class="form-control mr-sm-2" name="search" type="search" placeholder="Books search">
        </label>
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Books search</button>
    </form>

    <form action="/search_authors" method="GET" class="form-inline my-2 my-lg-0 p-2">
        <label>
            <input class="form-control mr-sm-2" name="search" type="search" placeholder="Authors search">
        </label>
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Authors search</button>
    </form>
    <form action="/sort_books_by_name" method="GET" class="form-inline my-2 my-lg-0 p-2">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Sort Books by Name</button>
    </form>

    <table class="table table-striped mt-3">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">ShortDescription</th>
            <th scope="col">Img</th>
            <th scope="col">Authors</th>
            <th scope="col">PubDate</th>
        </tr>
        </thead>
        <tbody>
        @foreach($books as $book)
            <tr>
                <th scope="row">{{ $book->book_id }}</th>
                <td>{{ $book->name }}</td>
                <td>{{ $book->short_description }}</td>
                <td><img width="100" height="100" src=" {{ asset('/storage/' . $book->img)}} " alt="Book-img"></td>
                <td>
                    @foreach($book->authors as $author)
                        {{ $author->surname . ' ' . $author->name . ' ' . $author->patronymic . '; '}}
                    @endforeach
                </td>
                <td>{{ $book->publication_date }}</td>
                <td class="table-buttons">

                    <button type="button" class="btn btn-success"
                            onClick=editBook("{{ route('Books.show', $book->book_id) }}")>
                        <i class="fa fa-eye" aria-hidden="true"></i>
                    </button>

                    <button type="button" class="btn btn-primary"
                            onClick=editBook("{{ route('Books.edit', $book->book_id) }}")>
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                    </button>

                    <form method="POST" action="{{ route('Books.destroy', $book->book_id) }}">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">
                            <i class="fa fa-trash"></i>
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach

        <div style="display: none" class="modal" id="editBookModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit book</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div id="modal-body" class="modal-body">

                    </div>
                </div>
            </div>
        </div>

        <script>
            function editBook(url) {
                $.ajax({
                    method: 'GET',
                    url: url,
                    data: {
                        dataType: 'json',
                        contentType: 'application/json',
                    }
                })
                    .done(function (data) {
                        $('div.modal-body').html(data);
                        $('#editBookModal').modal('show');
                    })
                    .fail(function () {
                        alert("error");
                    });
            }
        </script>
        </tbody>
    </table>
    {{ $books->links() }}

@endsection
