@extends('layouts.app')

@section('title', 'Add book')

@section('content')
    <nav class="navbar navbar-expand-lg navbar-dark bg-success mb-3 navbar-toggleable   ">
        <a class="navbar-brand" href="#">Book Reference</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02"
                aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item custom-control-inline">
                    <a class="nav-link" href="{{ route('Books.index') }}">Books</a>
                    <a class="nav-link" href="{{ route('Authors.index') }}">Authors</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="row">
        <div class="col-lg-6 mx-auto">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li> {{$error}}</li>
                        @endforeach
                    </ul>
                </div><br>
            @endif
            <form enctype="multipart/form-data" method="POST" action="{{ route('Books.store') }}">
                @csrf
                <div class="form-group">
                    <label for="book-name">Book's name</label>
                    <input required type="text" name="name" class="form-control" placeholder="Name" id="book-name"
                           value=" {{ old('name') }}">
                </div>
                <div class="form-group">
                    <label for="book-shortDescription">Short description</label>
                    <textarea class="form-control" name="short_description" rows="3" placeholder="Short description"
                              id="book-shortDescription">{{ old('short-description') }}</textarea>
                </div>
                <div>
                    <p>Upload img</p>
                    <p>
                        <input type="file" name="img" multiple accept="image/jpeg,image/png" id="book-img">
                    </p>
                </div>

                <label for="author">Select Authors:</label>
                <br>
                @foreach ($authors as $author)
                    <input type="checkbox" name="authors[]" id="author-id" value="{{ $author->author_id }}">
                    <label
                        for="author-id"> {{ $author->surname . ' ' .  $author->name . ' ' . $author->patronymic  }}</label>
                    <br>
                @endforeach


                <div class="form-group">
                    <label for="book-pubdate">Publication date</label>
                    <input required type="text" name="publication_date" class="form-control"
                           placeholder="Publication date" id="publication_date"
                           value=" {{ old('publication_date') }}">
                </div>
                <button type="submit" class="btn btn-success">Add book</button>
            </form>

        </div>
    </div
@endsection
