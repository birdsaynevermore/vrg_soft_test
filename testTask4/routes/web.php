<?php

    use Illuminate\Support\Facades\Route;

    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | contains the "web" middleware group. Now create something great!
    |
    */

    Route::get(
        '/',
        function () {
            return view('welcome');
        }
    );

//    Route::get(
//        '/Books',
//        function () {
//            return view('Books.index');
//        }
//    );

    Route::get('/search_books', 'BooksController@searchBooks');
    Route::get('/search_authors', 'BooksController@searchAuthors');
    Route::get('/sort_books_by_name', 'BooksController@sortByName');
    Route::get('/sort_authors_by_surname', 'AuthorsController@sortBySurname');
    Route::get('/search_author_name', 'AuthorsController@searchAuthorName');
    Route::get('/search_author_surname', 'AuthorsController@searchAuthorSurname');


    Route::resource('Books', 'BooksController');
    Route::resource('Authors', 'AuthorsController');




